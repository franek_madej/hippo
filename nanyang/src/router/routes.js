
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', name: 'Login', component: () => import('pages/index') },
      { path: 'availability', name: 'Availability', component: () => import('pages/availability') },
      { path: 'calendar', name: 'Calendar', component: () => import('pages/calendar') },
      { path: 'settings', name: 'Settings', component: () => import('pages/settings') }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
