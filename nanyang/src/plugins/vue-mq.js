import VueMq from 'vue-mq'

export default ({ Vue }) => {
  Vue.use(VueMq, {
    breakpoints: { // default breakpoints - customize this
      xs: 450,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: Infinity
    }
  })
}
