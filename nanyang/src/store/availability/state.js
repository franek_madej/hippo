import moment from 'moment'

export default {
  date: moment(),
  days: 7,
  startHour: 8,
  startMinute: 30,
  addHours: 1,
  addMinutes: 30,
  hourCount: 10,
  daysList: [],
  token: null,
  userID: null,
  userInfo: {
    firstName: '',
    lastName: '',
    email: '',
    kitNumber: 99,
    position: '',
    team: '',
    username: ''
  },
  teamSettings: {
    hour_count: 10,
    interval: '01:30:00',
    name: 'No team!',
    pk: 0,
    priority_days_ahead: 7,
    show_next_week: true,
    start_hour: '08:30:00',
    days_ahead: 28
  },
  settings: {
    teams: [],
    positions: []
  }
}
