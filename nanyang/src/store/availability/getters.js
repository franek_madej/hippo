export const getDate = (state) => {
  return state.date
}

export const getDateFormatted = (state) => {
  return state.date.valueOf()
}

export const loggedIn = (state) => {
  return Boolean(state.token && state.userID)
}

export const getToken = (state) => {
  return state.token
}

export const getUserID = (state) => {
  return state.userID
}

export const getUserInfo = (state) => {
  return state.userInfo
}

export const getTeamSettings = (state) => {
  return state.teamSettings
}

export const teamsAvailable = (state) => {
  return state.settings.teams.map(el => {
    return {
      label: el.name,
      value: el.pk
    }
  })
}

export const positionsAvailable = (state) => {
  return state.settings.positions.map(el => {
    return {
      label: el.name,
      value: el.pk
    }
  })
}

export const dailyAvailability = (state) => {
  return state.daysList
}
