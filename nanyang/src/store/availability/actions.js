import axios from 'axios'
import moment from 'moment'

import { cloneDeep, findIndex, forEach } from 'lodash'

let baseURL = '/api/v1'
let baseAuthURL = '/auth/'

if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
  const developmentServer = 'http://localhost:8000'
  baseURL = developmentServer + baseURL.slice(4, baseURL.length)
  baseAuthURL = developmentServer + baseAuthURL
}

export const getTeamSettings = ({ commit, state }) => {
  return new Promise((resolve, reject) => {
    axios.get(baseURL + `/teams/${state.userInfo.team}`)
      .then(response => {
        if (response.data.length) {
          commit('saveTeamSettings', response.data[0])
        }
        resolve(response)
      })
      .catch(error => {
        console.error(error)
        reject(error)
      })
  })
}

export const getUserInfo = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    axios.get(baseURL + `/users/?username=${payload.username}`)
      .then(response => {
        commit('saveUserID', response.data[0].id)
        commit('saveUserInfo', {
          username: response.data[0].username,
          first_name: response.data[0].first_name,
          last_name: response.data[0].last_name,
          email: response.data[0].email,
          kit_number: response.data[0].kit_number,
          position: response.data[0].position,
          team: response.data[0].team,
          locale: response.data[0].locale
        })
        resolve(response)
      })
      .catch(error => {
        console.error(error)
        reject(error)
      })
  })
}

export const changeUserInfo = ({ dispatch, commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    axios.put(baseURL + `/users/${state.userID}/`, payload)
      .then(() => dispatch('getUserInfo', payload)
        .then(() => resolve())
      )
      .catch(error => reject(error))
  })
}

export const changePassword = ({ state }, payload) => {
  return new Promise((resolve, reject) => {
    axios.post(baseAuthURL + `password/`, payload)
      .then(() => resolve())
      .catch(error => {
        reject(error.response.data)
      })
  })
}

export const getTeamsAvailable = ({ commit, state }) => {
  return new Promise((resolve, reject) => {
    axios.get(baseURL + `/teams/`)
      .then(response => {
        commit('saveTeamList', response.data)
        resolve()
      })
      .catch(error => reject(error))
  })
}

export const getPositionsAvailable = ({ commit, state }) => {
  return new Promise((resolve, reject) => {
    axios.get(baseURL + `/positions/`)
      .then(response => {
        commit('savePositionList', response.data)
        resolve()
      })
      .catch(error => reject(error))
  })
}

export const loginUser = ({ commit, dispatch, state }, payload) => {
  /*
   * payload: username and password
   */
  return new Promise((resolve, reject) => {
    axios.post(baseAuthURL + `token/create/`, payload)
      .then(response => {
        const token = response.data.auth_token
        commit('saveToken', token)
        dispatch('getUserInfo', payload)
          .then(() => {
            resolve()
          })
      })
      .catch(error => {
        reject(error)
      })
  })
}

export const logoutUser = ({ commit, state }) => {
  return new Promise((resolve, reject) => {
    axios.post(baseAuthURL + `token/destroy/`)
      .then(response => {
        commit('saveToken', null)
        commit('saveUserID', null)
        resolve()
      })
      .catch(error => reject(error))
  })
}

export const getDaysList = ({ state, commit }) => {
  const WEEK_LENGTH = 7
  const currentDate = moment(state.date)
  const listOfDays = []
  const priorityDue = moment(state.teamSettings.priority_fill_date)
  const due = moment(state.teamSettings.fill_date)
  for (let i = 0; i < state.days; i++) {
    const nextDate = moment(currentDate)
    listOfDays.push({
      date: nextDate,
      events: [],
      id: i,
      priorityDue: priorityDue > nextDate ? priorityDue : moment(priorityDue).add(WEEK_LENGTH, 'd'),
      due: due > nextDate ? due : moment(due).add(WEEK_LENGTH, 'd')
    })
    currentDate.add(1, 'd')
  }
  commit('saveDaysList', listOfDays)
}
export const getDailyAvailability = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    const promiseList = []
    const temporaryDaysList = cloneDeep(state.daysList)
    forEach(temporaryDaysList, day => {
      const now = moment.utc(day.date)
      now.hours(state.startHour)
      now.minutes(state.startMinute)
      const hourList = []
      for (let i = 0; i < state.hourCount; i++) {
        hourList.push({
          moment: moment.utc(now),
          date: now.format('YYYY-MM-DD'),
          time: now.format('HH:mm'),
          enabled: null,
          edited: false,
          people: []
        })
        now.add(state.addHours, 'h')
        now.add(state.addMinutes, 'm')
      }

      const url = baseURL + `/availablity/?date=${now.format('YYYY-MM-DD')}&player=${state.userID}`
      const axiosPromise = axios.get(url)
        .then(response => {
          if (response.data.length > 0) {
            response.data.forEach(el => {
              const time = el.time.slice(0, 5)
              const availablityIndex = findIndex(hourList, (hour) => hour.time === time)
              hourList[availablityIndex] = {
                date: el.date,
                time: time,
                enabled: el.available,
                edited: true
              }
            })
          }
          day.hourList = hourList
        })
        .catch(error => {
          console.error(error)
          reject(error)
        })
      promiseList.push(axiosPromise)
    })
    Promise.all(promiseList).then(() => {
      commit('saveDaysList', temporaryDaysList)
    })
      .then(() => {
        resolve()
      })
  })
}

export const getCaptainView = ({ commit, state, dispatch }) => {
  return new Promise((resolve, reject) => {
    dispatch('getDaysList')
      .then(() => {
        dispatch('getDailyAvailability')
          .then(() => {
            const promiseList = []
            const temporaryDaysList = cloneDeep(state.daysList)
            forEach(temporaryDaysList, day => {
              forEach(day.hourList, hour => {
                hour.people = []
                const url = baseURL + `/availablity/?date=${hour.date}&time=${hour.time}`
                const axiosPromise = axios.get(url)
                  .then(response => {
                    forEach(response.data, availablity => {
                      axios.get(baseURL + `/users/${availablity.player}/`)
                        .then(response => {
                          const firstName = response.data.first_name
                          const lastName = response.data.last_name
                          const kitNumber = response.data.kit_number
                          const username = response.data.username
                          const playerName = `${kitNumber}. ${firstName} ${lastName} (${username})`
                          hour.people.push({
                            available: availablity.available,
                            player: playerName
                          })
                        })
                    })
                  })
                  .catch(error => {
                    console.error(error)
                    reject(error)
                  })
                promiseList.push(axiosPromise)
              })
            })
            Promise.all(promiseList).then(() => {
              commit('saveDaysList', temporaryDaysList)
            })
            resolve()
          })
      })
  })
}

export const submitDays = ({ state }) => {
  return new Promise((resolve, reject) => {
    const promiseList = []
    forEach(state.daysList, daysList => {
      forEach(daysList.hourList, el => {
        const axiosPromise = axios.post(baseURL + '/availablity/', {
          'date': el.date,
          'time': el.time + ':00',
          'available': Boolean(el.enabled),
          'player': state.userID
        })
          .catch(error => {
            console.error(error)
            reject(error)
          })
        promiseList.push(axiosPromise)
      })
      Promise.all(promiseList).then(() => {
        resolve()
      })
    })
  })
}
