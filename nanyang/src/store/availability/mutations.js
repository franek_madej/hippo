import axios from 'axios'
import moment from 'moment'

import { findIndex } from 'lodash'

export const updateDate = (state, payload) => {
  state.date = moment(payload)
}

export const saveToken = (state, token) => {
  if (token) {
    axios.defaults.headers.common['Authorization'] = 'Token ' + token
  } else {
    delete axios.defaults.headers.common['Authorization']
  }
  state.token = token
}

export const saveUserID = (state, payload) => {
  state.userID = payload
}

export const saveUserInfo = (state, payload) => {
  state.userInfo = payload
}

export const saveTeamSettings = (state, payload) => {
  state.teamSettings = payload
}

export const setDaysCount = (state, payload) => {
  state.days = payload
}

export const saveDaysList = (state, payload) => {
  state.daysList = payload
}

export const saveTeamList = (state, payload) => {
  state.settings.teams = payload
}

export const savePositionList = (state, payload) => {
  state.settings.positions = payload
}

export const toggleOffDailyAvailability = (state, date) => {
  const dayIndex = findIndex(state.daysList, el => {
    return el.date.format('YYYY-MM-DD') === date
  })
  state.daysList[dayIndex].hourList = state.daysList[dayIndex].hourList.map(el => {
    el.enabled = false
    el.edited = true
    return el
  })
}

export const toggleOnDailyAvailability = (state, date) => {
  const dayIndex = findIndex(state.daysList, el => {
    return el.date.format('YYYY-MM-DD') === date
  })
  state.daysList[dayIndex].hourList = state.daysList[dayIndex].hourList.map(el => {
    el.enabled = true
    el.edited = true
    return el
  })
}

export const toggleAvailability = (state, payload) => {
  const dayIndex = findIndex(state.daysList, el => {
    return el.date.format('YYYY-MM-DD') === payload.date
  })
  state.daysList[dayIndex].hourList = state.daysList[dayIndex].hourList.map(el => {
    if (el.time === payload.time) {
      el.enabled = payload.value
      el.edited = true
    }
    return el
  })
}
