import Vue from 'vue'
import Vuex from 'vuex'

import availability from './availability'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    availability
  }
})

export default store
