![hippo logo](./media/logo_1024x1024.png)

# Hippo

> Your amateur team own hippocampus.

Make it remember when everybody can play. When they can practice. And when they
definetly can't. Hippo reminds, Hippo asks, Hippo delivers.

## Features

- Ask your teammates for availability

- Remind them about it weekly, and remind if they forget to fill it

- Schedule practices, matches, events and check who can make it

- Add event details

- Allow to choose best players, rate them, add their event stats

- See stats of players, match preformances

## Tech stack

- VueJS

- hmm...
